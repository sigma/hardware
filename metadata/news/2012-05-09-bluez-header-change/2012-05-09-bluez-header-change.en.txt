Title: bluez header change
Author: Wouter van Kesteren <woutershep@gmail.com>
Content-Type: text/plain
Posted: 2012-05-09
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: net-wireless/bluez[<4.99-r1]

Due to path changes, all packages that are dependent upon bluez must be
reinstalled after bluez is upgraded.

cave resolve --reinstall-dependents-of net-wireless/bluez nothing
