# Copyright 2013-2016 Pierre Lejeune <superheron@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require sourceforge udev-rules [ udev_files=[ src/92_pcscd_${PN}.rules ] ]

SUMMARY="ACS CCID PC/SC Driver for Linux/Mac OS X"
DESCRIPTION="
acsccid is a PC/SC driver for Linux/Mac OS X and it supports ACS CCID smart card readers.
This library provides a PC/SC IFD handler implementation and communicates with the readers
through the PC/SC Lite resource manager (pcscd).
"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        sys-devel/flex
        virtual/pkg-config
    build+run:
        dev-libs/libusb:1[>=1.0.9]
        sys-apps/pcsc-lite[>=1.8.3]
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-libusb
    --enable-pcsclite
    --enable-udev
    --disable-embedded
)

src_install() {
    default

    install_udev_files
}

