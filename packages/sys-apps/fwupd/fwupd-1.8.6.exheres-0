# Copyright 2017-2022 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=fwupd release=${PV} suffix=tar.xz ] \
    meson \
    python [ blacklist=2 multibuild=false ] \
    bash-completion \
    systemd-service \
    test-dbus-daemon \
    udev-rules \
    vala [ vala_dep=true with_opt=true ] \
    gtk-icon-cache

SUMMARY="A simple daemon to allow session software to update firmware"
HOMEPAGE+=" https://www.fwupd.org"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS="
    dell [[
        description = [ Support for flashing firmware on DELL machines ]
        requires = [ efi ]
    ]]
    efi [[ description = [ Support for flashing firmware via UEFI ] ]]
    firmware-packager [[ description = [ Python script to create firmware packages ] ]]
    fish-completion
    gobject-introspection
    gtk-doc
    logitech-bulk [[ description = [ Support for devices supporting the Logitech bulk controller protocol ] ]]
    modem-manager [[ description = [ Support for devices managed by ModemManager ] ]]
    sqlite
    synaptics-rmi [[ description = [ Support for Synaptics RMI devices ] ]]
    systemd
    tpm [[ description = [ Plugin exposing the system TPM device firmware version ] ]]
    uefi-pk [[ description = [ Support for checking UEFI Platform Keys ] ]]
    vapi [[ requires = [ gobject-introspection ] ]]
"

DEPENDENCIES="
    build:
        sys-apps/help2man
        sys-kernel/linux-headers[>=4.4-r1] [[ note = [ linux/nvme_ioctl.h for nvme plugin ] ]]
        virtual/pkg-config
        efi? (
            fonts/dejavu
            gnome-bindings/pygobject:3[python_abis:*(-)?][cairo]
            dev-python/pycairo[python_abis:*(-)?]
            media-libs/fontconfig
            media-libs/freetype:2
            x11-libs/cairo
            x11-libs/pango[gobject-introspection]
        )
        gtk-doc? ( dev-doc/gi-docgen[>=2021.1] )
        logitech-bulk? ( dev-libs/protobuf:* )
    build+run:
        app-arch/gcab[>=1.0] [[ note = [ colorhug and rpiupdate plugin, firmware-packager script ] ]]
        app-arch/libarchive[zstd]
        app-arch/libjcat[>=0.1.4][pkcs7]
        app-arch/xz
        core/json-glib[>=1.1.1]
        dev-libs/glib:2[>=2.58]
        dev-libs/libcbor[>=0.7.0]
        dev-libs/libgusb[>=0.3.0]
        dev-libs/libxmlb[>=0.1.13]
        dev-util/elfutils
        gnome-desktop/libgudev[>=232]
        net-misc/curl[>=7.62.0]
        sys-apps/diffutils
        sys-auth/polkit:1[>=0.113-r2] [[ note = [ itstools support ] ]]
        dell? (
            app-crypt/tpm2-tss[>=2.0]
            sys-apps/libsmbios[>=2.4.0]
        )
        efi? ( sys-boot/efivar[>=33] )
        gobject-introspection? ( gnome-desktop/gobject-introspection:1 )
        logitech-bulk? ( dev-libs/protobuf-c )
        modem-manager? (
            net-libs/libmbim[>=1.22.0]
            net-libs/libqmi[>=1.23.1]
            net-wireless/ModemManager[>=1.10.0]
        )
        sqlite? ( dev-db/sqlite:3 )
        synaptics-rmi? ( dev-libs/gnutls[>=3.6.0] )
        systemd? ( sys-apps/systemd[>=231] )
        tpm? ( app-crypt/tpm2-tss[>=2.0] )
        uefi-pk? ( dev-libs/gnutls[>=3.6.0] )
    run:
        efi? ( sys-apps/fwupd-efi )
        firmware-packager? (
            app-arch/p7zip
            dev-lang/python:*[>=3.5]
        )
    suggestion:
        net-wireless/bluez [[
            description = [ Support for updating bluetooth devices ]
        ]]
"

src_prepare() {
    meson_src_prepare

    # respect selected python abi
    edo sed -e "s/'python3'/'python$(python_get_abi)'/" -i meson.build

    # Skip a test which bails out complaining about sqlite support
    option sqlite || edo sed -e "/test('fu-self-test', /d" -i src/meson.build

    # Skip tests which want to connect to the systemd dbus
    edo sed -e "/test('linux-swap-self-test/d" -i plugins/linux-swap/meson.build
    edo sed -e "/test('redfish-self-test', /d" -i plugins/redfish/meson.build
}

_is_x86() {
    case "$(exhost --target)" in
        x86_64-* | i686-*)
            return 0
            ;;

        *)
            return 1
            ;;

    esac
}

src_configure() {
    local meson_args=(
        --localstatedir=/var
        -Dbluez=enabled
        -Dbuild=all
        -Dcbor=enabled
        -Dcompat_cli=true
        -Dconsolekit=enabled
        -Dcurl=enabled
        -Defi_binary=false
        -Defi_os_dir=exherbo
        -Delogind=disabled
        -Dgudev=enabled
        -Dgusb=enabled
        -Dhsi=enabled
        -Dlibarchive=enabled
        -Dlvfs=true
        -Dlzma=enabled
        -Dman=true
        -Dmetainfo=true
        -Dplugin_acpi_phat=enabled
        -Dplugin_amt=enabled
        -Dplugin_android_boot=enabled # needs gudev=enabled
        -Dplugin_bcm57xx=enabled   # needs gudev=enabled
        -Dplugin_cfu=enabled   # needs gudev=enabled
        -Dplugin_cpu=enabled
        -Dplugin_dummy=false
        -Dplugin_emmc=enabled
        -Dplugin_ep963x=enabled   # needs gudev=enabled
        -Dplugin_fastboot=enabled  # needs gusb=enabled
        -Dplugin_flashrom=disabled
        -Dplugin_gpio=enabled   # needs gudev=enabled
        -Dplugin_mtd=enabled
        -Dplugin_nitrokey=enabled
        -Dplugin_nvme=enabled
        -Dplugin_parade_lspcon=enabled
        -Dplugin_pixart_rf=enabled   # needs gudev=enabled
        -Dplugin_powerd=enabled
        -Dplugin_realtek_mst=enabled
        -Dplugin_redfish=enabled
        -Dplugin_synaptics_mst=enabled
        -Dplugin_scsi=enabled   # needs gudev=enabled
        -Dplugin_uf2=enabled   # needs gusb=enabled
        -Dplugin_upower=enabled
        -Dpolkit=enabled
        -Dqubes=false
        -Dsoup_session_compat=true
        -Dstatic_analysis=false
        -Dsystemd_root_prefix=/usr/$(exhost --target)
        -Dudevdir=${UDEVDIR}

        $(meson_feature efi plugin_uefi_capsule)
        $(meson_feature dell plugin_dell)
        $(meson_feature gobject-introspection introspection)
        $(meson_feature gtk-doc docs)
        $(meson_feature logitech-bulk plugin_logitech_bulkcontroller)
        $(meson_feature modem-manager plugin_modem_manager)
        $(meson_feature sqlite)
        $(meson_feature synaptics-rmi plugin_synaptics_rmi)
        $(meson_feature systemd)
        $(meson_feature systemd offline)
        $(meson_feature tpm plugin_tpm)
        $(meson_feature uefi-pk plugin_uefi_pk)

        $(meson_switch bash-completion bash_completion)
        $(meson_switch efi plugin_uefi_capsule_splash)
        $(meson_switch firmware-packager)
        $(meson_switch fish-completion fish_completion)

        $(expecting_tests -Dtests=true -Dtests=false)
    )

    if option synaptics-rmi || option uefi-pk ; then
        meson_args+=( '-Dgnutls=enabled' )
    else
        meson_args+=( '-Dgnutls=disabled' )
    fi

    if _is_x86; then
        meson_args+=(
            -Dplugin_intel_spi=true
            -Dplugin_msr=enabled
        )
    else
        meson_args+=(
            -Dplugin_intel_spi=false
            -Dplugin_msr=disabled
        )
    fi

    exmeson "${meson_args[@]}"
}

src_test() {
    esandbox allow_net "unix:${WORK}/(null)/ioctl/_default"
    esandbox allow_net --bind "unix:${TEMP}/umockdev.*/ioctl/_default"
    esandbox allow_net --bind "unix:${TEMP}/umockdev.*/event*"

    test-dbus-daemon_run-tests meson_src_test

    esandbox disallow_net "unix:${WORK}/(null)/ioctl/_default"
    esandbox disallow_net --bind "unix:${TEMP}/umockdev.*/ioctl/_default"
    esandbox disallow_net --bind "unix:${TEMP}/umockdev.*/event*"
}

src_install() {
    meson_src_install

    keepdir /var/lib/fwupd
}

