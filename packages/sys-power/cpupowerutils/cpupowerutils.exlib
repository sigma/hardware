# Copyright 2011 Johannes Nixdorf
# Distributed under the terms of the GNU General Public License v2

require systemd-service

export_exlib_phases src_install

SUMMARY="Userspace utilities for the Linux kernel cpufreq subsystem"
HOMEPAGE="https://www.kernel.org"
DOWNLOADS="mirror://kernel/linux/kernel/v$(ever major).x/linux-${PV}.tar.xz"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        sys-apps/pciutils
"

WORK=${WORKBASE}/linux-${PV}/tools/power/cpupower

DEFAULT_SRC_COMPILE_PARAMS=(
    CROSS=$(exhost --tool-prefix)
)

DEFAULT_SRC_INSTALL_PARAMS=(
    docdir=/usr/share/doc/${PNVR}
    libdir=/usr/$(exhost --target)/lib
    mandir=/usr/share/man
    bindir=/usr/$(exhost --target)/bin
    sbindir=/usr/$(exhost --target)/bin
    includedir=/usr/$(exhost --target)/include
)

cpupowerutils_src_install() {
    default

    hereconfd cpupower.conf <<EOF
# See 'cpupower help' and cpupower(1) for more info
CPUPOWER_START_OPTS="frequency-set -g ondemand"
CPUPOWER_STOP_OPTS="frequency-set -g performance"
EOF

    install_systemd_files
}

