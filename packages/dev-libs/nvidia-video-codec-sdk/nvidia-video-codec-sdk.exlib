# Copyright 2016 Jorge Aparicio
# Distributed under the terms of the GNU General Public License v2

myexparam cuda_version
myexparam driver_version

exparam -v CUDA_VERSION cuda_version
exparam -v DRIVER_VERSION driver_version

SUMMARY="NVIDIA Video Codec SDK"
DESCRIPTION="
The SDK consists of two hardware acceleration interfaces:

* NVENCODE API for video encode acceleration
* NVDECODE API for video decode acceleration (formerly called NVCUVID API)

NVIDIA GPUs contain one or more hardware-based decoder and encoder(s) (separate from the CUDA
cores) which provides fully-accelerated hardware-based video decoding and encoding for several
popular codecs. With decoding/encoding offloaded, the graphics engine and the CPU are free for
other operations.

GPU hardware accelerator engines for video decoding (referred to as NVDEC) and video encoding
(referred to as NVENC) support faster than real-time video processing which makes them suitable
to be used for transcoding applications, in addition to video playback.
"
HOMEPAGE="https://developer.nvidia.com/nvidia-video-codec-sdk"
DOWNLOADS="
    manual: Video_Codec_SDK_${PV}.zip
"

LICENCES="
    MIT [[ note = [ headers ] ]]
    NVIDIA-VIDEO-CODEC-SDK
"
SLOT="0"
MYOPTIONS=""

RESTRICT="fetch test"

DEPENDENCIES="
    build:
        virtual/unzip
    run:
        dev-util/nvidia-cuda-toolkit[>=${CUDA_VERSION}]
        x11-drivers/nvidia-drivers[>=${DRIVER_VERSION}]
"

WORK="${WORKBASE}"/Video_Codec_SDK_${PV}

src_install() {
    insinto /usr/$(exhost --target)/include
    if ever at_least 10.0.26; then
        doins Interface/*
    else
        doins include/*
    fi
}

